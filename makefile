UPDATE :
	cp .env.development .env
RUN : UPDATE
	docker-compose up --build
INSTALL : UPDATE
	docker-compose run web yarn
MIGRATION : UPDATE
	docker-compose exec web yarn migration:run
print : UPDATE
	docker-compose run db printenv

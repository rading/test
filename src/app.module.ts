import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import configModuleOptions from '@app/config/config.options';
import { HealthModule } from '@app/modules/health/health.module';
import { NoteModule } from '@app/modules/note/note.module';

@Module({
  imports: [
    HealthModule,
    NoteModule,
    ConfigModule.forRoot(configModuleOptions),
    TypeOrmModule.forRootAsync({
      useFactory: async (config: ConfigService) => config.get('database'),
      inject: [ConfigService],
    }),
  ],
})
export class AppModule {}
